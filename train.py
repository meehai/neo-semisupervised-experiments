#!/usr/bin/env python3
from pathlib import Path

from argparse import ArgumentParser, Namespace
from omegaconf import OmegaConf, DictConfig
from functools import partial
import torch as tr
import numpy as np
import pickle

from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotMetrics, CopyBestCheckpoint
from lightning_module_enhanced.train_setup import TrainSetup
from lightning_module_enhanced.utils import to_device, to_tensor
from lightning_module_enhanced.multi_trainer import MultiTrainer
from loguru import logger
from ngclib.graph_cfg import NGCNodesImporter
from torch.utils.data import DataLoader
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger
from pytorch_lightning.callbacks import ModelCheckpoint

from reader import ConcatReader
from models import build_model
from utils import accelerator_params_from_cfg, get_experiment_dir

# train algorithms

def loss_fn_w(y: tr.Tensor, gt: tr.Tensor, w: tr.Tensor) -> tr.Tensor:
    """masked by variance l2"""
    mask = gt != 0
    y, gt, w = y[mask], gt[mask], w[mask]
    return (w * (y - gt).pow(2)).mean()

def semisup_hard_threshold(model: LME, train_batch: dict, hard_threshold: float, consistency: dict[str, tr.Tensor],
                           prefix: str = "") -> tr.Tensor:
    if prefix != "":
        return LME.feed_forward_algorithm(model, train_batch, prefix)
    x = train_batch["data"]
    w_batch = tr.ones_like(x)
    for i in range(len(x)):
        if train_batch["name"][i] in consistency.keys():
            # binarize here
            w_batch[i] = consistency[train_batch["name"][i]] <= hard_threshold

    assert isinstance(x, (dict, tr.Tensor)), type(x)
    y = model.forward(x)
    gt = to_device(to_tensor(train_batch["labels"]), model.device)
    res = model.lme_metrics(y, gt, prefix, include_loss=False)
    res["loss"] = loss_fn_w(y, gt, w_batch)
    return res

def semisup_soft_threshold(model: LME, train_batch: dict, soft_threshold: tuple[float, float],
                           consistency: dict[str, tr.Tensor], prefix: str = "") -> tr.Tensor:
    if prefix != "":
        return LME.feed_forward_algorithm(model, train_batch, prefix)
    assert len(soft_threshold) == 2, soft_threshold
    a, b = soft_threshold
    x = train_batch["data"]
    w_batch = tr.ones_like(x)
    for i in range(len(x)):
        if train_batch["name"][i] in consistency.keys():
            # exp(a * var(x) + b) here
            var_x = consistency[train_batch["name"][i]]
            w_batch[i] = tr.exp(var_x * a + b)

    assert isinstance(x, (dict, tr.Tensor)), type(x)
    y = model.forward(x)
    gt = to_device(to_tensor(train_batch["labels"]), model.device)
    res = model.lme_metrics(y, gt, prefix, include_loss=False)
    res["loss"] = loss_fn_w(y, gt, w_batch)
    return res

def _get_train_val_loaders(cfg: DictConfig, args: Namespace, nodes: list) -> (DataLoader, DataLoader):
    output_nodes = [n for n in nodes if n.name not in cfg.nodes.input_nodes]
    train_reader = ConcatReader(path=args.train_set_path, nodes=nodes, out_nodes=output_nodes)
    val_reader = ConcatReader(path=args.validation_set_path, nodes=nodes, out_nodes=output_nodes)
    collate_fn = train_reader.collate_fn
    val_loader = DataLoader(val_reader, **{**cfg.data.loader_params, "shuffle": False}, collate_fn=collate_fn)
    train_loader = DataLoader(train_reader, **cfg.data.loader_params, collate_fn=collate_fn)
    return train_loader, val_loader

def get_args():
    """CLI args"""
    parser = ArgumentParser()
    parser.add_argument("mode", help="Which train mode to run. Must be in cfg.train node. Use 'supervised' as default.")
    parser.add_argument("--train_set_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--config_path", type=Path, required=True, help="Path to the config path")
    parser.add_argument("--validation_set_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--experiment_dir", type=Path, help="Path to the experiments dir. Defaults to 'experiments'")
    parser.add_argument("--nodes_path", type=Path, help="Path to the nodes implementation used to create the graph")
    parser.add_argument("--num_trains", type=int, default=1, help="Number of times we train the model")
    args = parser.parse_args()
    assert args.num_trains > 0, "num_trains must be > 0"
    return args

def main():
    """main fn"""
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    assert args.mode in cfg.train, f"Mode '{args.mode}' not found in cfg.train '{cfg.train.keys()}'"
    nodes = NGCNodesImporter(cfg.nodes.names, cfg.nodes.types, cfg.nodes.hyper_parameters,
                             nodes_module_path=args.nodes_path).nodes
    output_nodes = [n for n in nodes if n.name not in cfg.nodes.input_nodes]
    assert len(output_nodes) == 1, f"Only 1 output node is supported for now: {output_nodes}"
    experiment_dir = get_experiment_dir(args, cfg)
    logger.info(f"This experiment dir: '{experiment_dir}'")

    ckpt_path = experiment_dir / args.mode / "checkpoints/model_best.ckpt"
    if ckpt_path.exists():
        logger.warning(f"Supervised checkpoint: '{ckpt_path}' exists. Returning early.")
        return

    train_loader, val_loader = _get_train_val_loaders(cfg, args, nodes)
    # assumption that each node has 1 channel
    model = LME(build_model(cfg, len(cfg.nodes.input_nodes), len(output_nodes))).to("cpu")
    print(model.summary)

    # run the training
    model.criterion_fn = output_nodes[0].criterion_fn
    model.metrics = output_nodes[0].metrics
    if args.mode == "semisupervised_hard_threshold":
        file_name = cfg.train.get(args.mode).threshold_name
        hard_threshold = np.load(args.train_set_path / output_nodes[0].name / "hard_threshold.npy")[0]
        consistency = pickle.load(open(args.train_set_path / output_nodes[0].name / "consistency.pickle", "rb"))
        consistency = {consistency["names"][i]: tr.from_numpy(consistency["cons"][i])
                       for i in range(len(consistency["names"]))}
        model.model_algorithm = partial(semisup_hard_threshold, hard_threshold=hard_threshold, consistency=consistency)
        logger.info(f"Loaded hard threshold and consistency from '{file_name}'")

    if args.mode == "semisupervised_soft_threshold":
        file_name = cfg.train.get(args.mode).threshold_name
        soft_threshold = np.load(args.train_set_path / output_nodes[0].name / "soft_threshold.npy").tolist()
        consistency = pickle.load(open(args.train_set_path / output_nodes[0].name / "consistency.pickle", "rb"))
        consistency = {consistency["names"][i]: tr.from_numpy(consistency["cons"][i])
                       for i in range(len(consistency["names"]))}
        model.model_algorithm = partial(semisup_soft_threshold, soft_threshold=soft_threshold, consistency=consistency)
        logger.info(f"Loaded soft threshold and consistency from '{file_name}'")

    if args.mode in ("semisupervised_hard_percentile_threshold", "semisupervised_below_line_percentile_threshold"):
        file_name = cfg.train.get(args.mode).threshold_name
        thresholds = np.load(args.train_set_path / output_nodes[0].name / file_name, allow_pickle=True).item()
        consistency = pickle.load(open(args.train_set_path / output_nodes[0].name / "consistency.pickle", "rb"))
        consistency = {consistency["names"][i]: tr.from_numpy(consistency["cons"][i])
                       for i in range(len(consistency["names"]))}
        assert cfg.train.get(args.mode).percentile in thresholds, thresholds
        hard_threshold = thresholds[cfg.train.get(args.mode).percentile]
        model.model_algorithm = partial(semisup_hard_threshold, hard_threshold=hard_threshold, consistency=consistency)
        logger.info(f"Loaded percentile thresholds and consistency from '{file_name}'")

    model.reset_parameters(cfg.seed)
    model.callbacks = [ModelCheckpoint(save_last=True, save_top_k=1, monitor="val_loss"),
                       PlotMetrics(),
                       CopyBestCheckpoint()]
    TrainSetup(model, OmegaConf.to_container(cfg.train))
    acc_params = accelerator_params_from_cfg(cfg)
    pl_logger = CSVLogger(save_dir=experiment_dir, name=args.mode, version="")
    trainer = Trainer(logger=pl_logger, **cfg.train.trainer_params, **acc_params)
    if args.num_trains > 1:
        trainer = MultiTrainer(trainer, num_trains=args.num_trains, n_devices=-1)
    trainer.fit(model, train_loader, val_loader)

if __name__ == "__main__":
    main()
