from ngclib.readers import NGCNpzReader
import torch as tr

class ConcatReader(NGCNpzReader):
    """
    Wrapper on top of NGCNpzReader to concatenate all inputs/outputs in two hypernodes for a simple network.
    Also works fine for 1 in, 1 out. It removs the keys from the dict, so we can use LME directly.
    """
    def collate_fn(self, batch: list) -> dict:
        res = super().collate_fn(batch)
        x = tr.cat([x for x in res["data"].values()], axis=-1)
        gt = tr.cat([x for x in res["labels"].values()], axis=-1) if res["labels"] != {} else {}
        return {"data": x, "labels": gt, "name": res["name"]}
