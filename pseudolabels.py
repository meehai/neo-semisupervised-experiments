#!/usr/bin/env python3
"""simple pseudolabels algorithm. Takes a pre-trained model and generates pseudo-labels for the semi-supervised data"""
from argparse import ArgumentParser
from pathlib import Path
import os
from omegaconf import DictConfig, OmegaConf
from tqdm import tqdm
from lightning_module_enhanced import LME
from torch.utils.data import DataLoader
from ngclib.models import NGCNode
from ngclib.graph_cfg import NGCNodesImporter
from ngclib.logger import logger
import numpy as np
import torch as tr

from reader import ConcatReader
from models import build_model

@tr.no_grad()
def do_pseudolabels(model: LME, cfg: DictConfig, nodes: list[NGCNode], experiment_dir: Path,
                    semisupervised_reader: ConcatReader, pseudolabels_dir_name: str = "pseudolabels"):
    """
    pseudolabels for semi-supervised training

    Parameters
    - model The model
    - cfg The config
    - nodes The nodes
    - experiment_dir The experiment dir where the model was trained
    - semisupervised_reader The semi-supervised reader (just input nodes)
    - pseudolabels_dir_name - Defaults to 'pseduolabels', but we can also export later on with some other algos.
    Also, we can export pseudolabels on train set.
    """
    pseudolabels_dir = experiment_dir / pseudolabels_dir_name
    pseudolabels_dir.mkdir(exist_ok=True)
    # create a reader from said input data
    in_nodes, out_nodes = semisupervised_reader.in_nodes, [n for n in nodes if n.name not in cfg.nodes.input_nodes]
    in_names, out_names = [n.name for n in in_nodes], [n.name for n in out_nodes]
    semisupervised_loader = DataLoader(semisupervised_reader, **{**cfg.data.loader_params, "shuffle": False},
                                       collate_fn=semisupervised_reader.collate_fn)
    logger.info(f"Generating pseudolabels. Input nodes: {in_names}, output nodes: {out_names}")

    # symlink the semi-supervised input nodes
    for node in in_nodes:
        out_path = pseudolabels_dir / node.name
        out_path.mkdir(exist_ok=True)
        for item in (semisupervised_reader.path / node.name).iterdir():
            if not (out_path / item.name).exists():
                os.symlink(item, out_path / item.name)

    lens = []
    for node in out_nodes:
        out_path = pseudolabels_dir / node.name
        out_path.mkdir(exist_ok=True)
        lens.append(len(list(out_path.iterdir())))

    assert all(l == 0 for l in lens) or all(l != 0 for l in lens), lens
    if all(l != 0 for l in lens):
        assert all(l == lens[0] for l in lens), lens
        logger.debug(f"All pseudolabels are already genearated at '{pseudolabels_dir}'. Skipping.")
        return

    # generate pseudo-labels for output nodes
    for item in tqdm(semisupervised_loader, desc="pseudolabels"):
        x = item["data"]
        y: tr.Tensor = model.forward(x)
        y_np: np.ndarray = y.to("cpu").numpy()
        # store the pseudolabels to disk (app specific stuff here, we know they are concatenated)
        for i, node in enumerate(out_nodes):
            for b in range(len(y)):
                out_path = pseudolabels_dir / node.name / f"{item['name'][b]}.npz"
                node_data: tr.Tensor = node.save_to_disk(y_np[b, ..., i])
                np.savez(out_path, node_data)

    logger.info(f"Stored pseudolabels at '{pseudolabels_dir}'")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--semisupervised_set_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--config_path", type=Path, required=True, help="Path to the config path")
    parser.add_argument("--experiment_dir", type=Path, required=True,
                        help="Path to the experiments dir. Defaults to 'experiments'")
    parser.add_argument("--nodes_path", type=Path, help="Path to the nodes implementation used to create the graph")
    parser.add_argument("--pseudolabels_dir_name", default="pseudolabels", help="Name of the pseudolabels dir")
    args = parser.parse_args()
    assert args.experiment_dir.exists(), f"Experiment dir '{args.experiment_dir}' does not exist"
    assert (args.experiment_dir / "supervised").exists(), f"'supervised' doesn't exist in '{args.experiment_dir}'"
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    logger.info(f"This experiment dir: '{args.experiment_dir}'")
    nodes = NGCNodesImporter(cfg.nodes.names, cfg.nodes.types, cfg.nodes.hyper_parameters,
                             nodes_module_path=args.nodes_path).nodes
    in_nodes = [n for n in nodes if n.name in cfg.nodes.input_nodes]
    semisupervised_reader = ConcatReader(path=args.semisupervised_set_path, nodes=in_nodes, out_nodes=[])
    model = LME(build_model(cfg, len(in_nodes), len(nodes) - len(in_nodes))).to("cpu")
    model.load_state_from_path(args.experiment_dir / "supervised/checkpoints/model_best.ckpt")
    do_pseudolabels(model, cfg, nodes, args.experiment_dir, semisupervised_reader, args.pseudolabels_dir_name)


if __name__ == "__main__":
    main()
