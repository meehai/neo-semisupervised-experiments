#!/usr/bin/env python3
from argparse import ArgumentParser
from pathlib import Path
from natsort import natsorted
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import colormaps as cmaps
from numpy.typing import NDArray
from sklearn.linear_model import LinearRegression
from media_processing_lib.image import image_write
import pickle
from loguru import logger

def get_best_performance_edge_dir(dump_dir: Path, it1_results_dir: Path, node: str) -> Path:
    """given a dump dir, a node and the path to the csv results file, return the path to the node's dump data"""
    csv_file_path = it1_results_dir / f"{node}_results_edges_test.csv"
    assert csv_file_path.exists(), csv_file_path
    df = pd.read_csv(csv_file_path).sort_values("l2")
    best_performing_edge = df[df["edges"].str.startswith("Single ")]["edges"].iloc[0]
    best_performing_edge_dir = dump_dir / node / "0" / best_performing_edge / "npy"
    assert best_performing_edge_dir.exists(), best_performing_edge_dir
    return best_performing_edge_dir

# @cache_fn(NpyFS, key_encode_fn=lambda res_dir: hashlib.md5(str(res_dir.absolute()).encode("utf-8")).hexdigest())
def read_from_dir(res_dir: Path) -> NDArray["n,h,w,c"]:
    assert res_dir.exists(), res_dir
    files = natsorted([x for x in res_dir.glob("*.npz")], key=lambda p: p.name)
    assert len(files) > 0, res_dir
    data = np.array([np.load(x)["arr_0"] for x in files])
    data = np.expand_dims(data, axis=-1) if len(data.shape) == 3 else data
    return data, files

def cons(data: NDArray["k,h,w,c"]) -> NDArray["h,w,c"]:
    """consistency for 1 volumne, cented in 1 data point. For start/end time points, the volume can be even too"""
    res = np.std(data, axis=0)
    return res

def compute_consistency(y_vol: NDArray["n,h,w,c"], k: int) -> NDArray["n,h,w,c"]:
    """Compute consistency between -k/+k neighboring frames"""
    logger.info(f"Computing consistency (k={k})")
    timespan = y_vol.shape[0]
    consistency_array = []
    for index in range(timespan):
        start_idx = max(0, index - k)
        end_idx = min(timespan, index + k + 1)
        cons_map = cons(y_vol[start_idx: end_idx])
        consistency_array.append(cons_map)
    assert len(consistency_array) == timespan
    res = np.array(consistency_array)
    return res

def _get_cons(predictions_dir: Path, y_data: NDArray["n,h,w,c"], y_files: list, cons_k: int) -> NDArray["n,h,w,c"]:
    cons_file = predictions_dir / "consistency.pickle"
    y_files_stem = tuple(x.stem for x in y_files)
    if not cons_file.exists():
        cons_all = compute_consistency(y_data, k=cons_k)
        pickle.dump({"cons": cons_all, "names": y_files_stem}, open(cons_file, "wb"))
        logger.info(f"Saved consistency to '{cons_file}'")
    else:
        logger.info(f"Loading consistency from '{cons_file}'")
        cons_all = pickle.load(open(cons_file, "rb"))["cons"]
    return cons_all

def l2(y: NDArray["h,w,c"], gt: NDArray["h,w,c"]) -> NDArray["h,w,c"]:
    """l2 for 1 item -- returns a map with nans on invalid pixels"""
    assert len(y.shape) == 3 and len(gt.shape) == 3, (y.shape, gt.shape)
    y_orig_shape = y.shape
    y = y.flatten().astype(float)
    gt = gt.flatten().astype(float)
    mask = gt != 0
    result = np.full_like(y, np.nan)
    result[mask] = (y[mask] - gt[mask]) ** 2
    result = result.reshape(y_orig_shape)
    return result

def compute_l2(y_data: NDArray["n,h,w,c"], gt_data: NDArray["n,h,w,c"]) -> NDArray["n,h,w,c"]:
    """Appies the metric_fn for the (y, gt) data. Returns the per-map score."""
    logger.info("Computing L2")
    assert len(y_data) == len(gt_data), (len(y_data), len(gt_data))
    res = np.zeros_like(y_data, dtype=np.float32)
    for i, (y, gt) in enumerate(zip(y_data, gt_data)):
        res[i] = l2(y, gt)
    logger.info(f"Computed L2 for {len(y_data)} maps. Invalid pixels: "
                f"{np.isnan(res).sum() / np.prod(res.shape) * 100:.2f}%")
    return res

def _get_freqs(x: NDArray["n"], y: NDArray["n"], n_bins: int) -> (NDArray["n_bins,b_nins"],
                                                                  (NDArray["n_bins"], NDArray["n_bins"])):
    assert len(x) == len(y) and len(x.shape) == 1, (x.shape, y.shape)
    x_ls = np.linspace(0, x.max(), n_bins)
    y_ls = np.linspace(0, y.max(), n_bins)
    x_digit = np.digitize(x, x_ls) - 1
    y_digit = np.digitize(y, y_ls) - 1
    uniqs, freqs = np.unique(np.stack([x_digit, y_digit], axis=1), return_counts=True, axis=0)
    res = np.zeros((n_bins, n_bins), dtype=np.int64)
    res[uniqs[:, 0], uniqs[:, 1]] = freqs
    return res, (x_ls, y_ls)

def _get_l2_cons_valid(l2_all: NDArray["n,h,w,c"], cons_all: NDArray["n,h,w,c"],
                       n_sample: int) -> (NDArray["m,h,w,c"], NDArray["m,h,w,c"]):
    l2_all = l2_all.flatten()
    cons_all = cons_all.flatten()
    mask = np.isnan(l2_all)
    l2_valid = l2_all[~mask]
    cons_valid = cons_all[~mask]

    if n_sample is not None:
        idx = np.random.choice(len(l2_valid), n_sample, replace=False)
        l2_valid = l2_valid[idx]
        cons_valid = cons_valid[idx]

    cons_valid = np.clip(cons_valid, 0, np.percentile(cons_valid, 99))
    l2_valid = np.clip(l2_valid, 0, np.percentile(l2_valid, 99))
    return l2_valid, cons_valid

def _do_plot(x: NDArray["n,h,w,c"], y: NDArray["n,h,w,c"], x_bins: NDArray["n_bins"], out_path: Path, title: str,
             xlabel: str, ylabel: str, enable_scatter: bool = True):
    x_ls = np.linspace(0, x.max(), len(x))
    y_avg_per_bin = np.zeros_like(y)
    n_bins = len(x_bins)
    for i in range(n_bins):
        l, r = len(x) * i // n_bins, len(x) * (i + 1) // n_bins
        cond = x >= x_bins[i]
        cond = cond & (x < x_bins[i + 1]) if i < n_bins - 1 else cond
        scores = y[cond]
        assert len(scores) > 0
        y_avg_per_bin[l:r] = scores.mean()

    plt.clf()
    if enable_scatter:
        plt.plot(x, y, "o")
    plt.plot(x_ls, y.mean() * np.ones_like(y), "-", label="mean")
    plt.plot(x_ls, y_avg_per_bin, "-", label="mean_per_bin")
    if title is not None:
        plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(out_path, bbox_inches="tight")
    logger.info(f"Saved at '{out_path}'")

def plot_cons_vs_err(l2_all: NDArray["n,h,w,c"], cons_all: NDArray["n,h,w,c"], out_dir: Path,
                     n_sample: int = None, plot_title: str = None, enable_scatter: bool = True,
                     n_bins: int = 10):
    """Plot the error vs consistency on the valid pixels only"""
    logger.info("Plotting cons vs err and err vs cons")
    if (out_dir / "cons_vs_l2.png").exists() and (out_dir / "l2_vs_cons.png").exists():
        logger.info("Already plotted. Skipping")
        return

    l2_valid, cons_valid = _get_l2_cons_valid(l2_all, cons_all, n_sample)
    _, bins = _get_freqs(cons_valid, l2_valid, n_bins=n_bins)
    _do_plot(cons_valid, l2_valid, bins[0], out_dir / "cons_vs_l2.png", title=plot_title,
             xlabel="Standard deviation of neighboring months", ylabel="L2 error (on valid pixels)",
             enable_scatter=enable_scatter)
    _do_plot(l2_valid, cons_valid, bins[1], out_dir / "l2_vs_cons.png", title=plot_title,
             ylabel="Standard deviation of neighboring months", xlabel="L2 error (on valid pixels)",
             enable_scatter=enable_scatter)

def compute_bins_on_freqs(l2_all: NDArray["n,h,w,c"], cons_all: NDArray["n,h,w,c"], n_sample: int,
                          n_bins: int) -> (NDArray["n_bins,n_bins"], NDArray["2,n_bins"], NDArray["2,n_bins"]):
    l2_valid, cons_valid = _get_l2_cons_valid(l2_all, cons_all, n_sample)
    freqs, (bins_cons, bins_l2) = _get_freqs(cons_valid, l2_valid, n_bins=n_bins)

    avg_l2_per_cons_bin = np.zeros((n_bins, ), dtype=np.float32)
    for i in range(n_bins):
        cond = cons_valid >= bins_cons[i]
        cond = cond & (cons_valid < bins_cons[i + 1]) if i < n_bins - 1 else cond
        scores = l2_valid[cond]
        assert len(scores) > 0
        avg_l2_per_cons_bin[i] = scores.mean()

    avg_cons_per_l2_bin = np.zeros((n_bins, ), dtype=np.float32)
    for i in range(n_bins):
        cond = l2_valid >= bins_l2[i]
        cond = cond & (l2_valid < bins_l2[i + 1]) if i < n_bins - 1 else cond
        scores = cons_valid[cond]
        assert len(scores) > 0
        avg_cons_per_l2_bin[i] = scores.mean()

    return freqs, (bins_cons, bins_l2), (avg_l2_per_cons_bin, avg_cons_per_l2_bin)

def _get_bins_on_freqs(predictions_dir: Path, *args, **kwargs) \
        -> (NDArray["n_bins,n_bins"], NDArray["2,n_bins"], NDArray["2,n_bins"]):
    assert 'n_bins' in kwargs
    file_path = predictions_dir / f"freqs_bins_avgs_nbins_{kwargs['n_bins']}.pickle"
    if not file_path.exists():
        freqs, bins, avgs = compute_bins_on_freqs(*args, **kwargs)
        data = {"freqs": freqs, "bins": bins, "avgs": avgs}
        pickle.dump(data, open(file_path, "wb"))
        logger.info(f"Saved freqs, bins, avgs to '{file_path}'")
    else:
        logger.info(f"Loading freqs, bins, avgs from '{file_path}'")
        data = pickle.load(open(file_path, "rb"))
        freqs, bins, avgs = data["freqs"], data["bins"], data["avgs"]
    return freqs, bins, avgs

def cons_and_error_magnitude(l2_all: NDArray["n,h,w,c"], cons_all: NDArray["n,h,w,c"], out_dir: Path):
    logger.info(f"computing consistency x error magnitude and storing images to '{out_dir}'")
    hot = cmaps["hot"]
    out_dir.mkdir(parents=True, exist_ok=True)
    mask = np.isnan(l2_all)
    l2_valid, cons_valid = l2_all[~mask], cons_all[~mask]
    cons_limits = np.percentile(cons_valid, [1, 99])
    l2_limits = np.percentile(l2_valid, [1, 99])

    hyp = np.hypot(l2_all.clip(*l2_limits), cons_all.clip(*cons_limits)).squeeze()
    _min, _max = np.nanmin(hyp), np.nanmax(hyp)
    hyp_norm = (hyp - _min) / (_max - _min)
    hyp_hot = (hot(hyp_norm)[..., 0:3] * 255).astype(np.uint8)
    for i in range(len(hyp_hot)):
        image_write(hyp_hot[i], out_dir / f"{i}.png")

def compute_hard_threshold(l2_all: NDArray["n,h,w,c"], avg_l2_per_cons_bin: NDArray["n_bins"],
                           bins_cons: NDArray["n_bins"]) -> float:
    """
    Computes the hard threshold for semi-supervised learning by looking at the average L2 error per bin vs the
    average error overall.
    """
    logger.info("Computing hard threshold")
    l2_valid = l2_all[~np.isnan(l2_all)]
    X = l2_valid.mean() > avg_l2_per_cons_bin
    first_ix = np.where(X == 0)[0]
    assert len(first_ix) > 0, X
    first_ix = first_ix[0]
    first_cons = bins_cons[first_ix]
    return first_cons

def get_hard_threshold(predictions_dir: Path, *args, **kwargs) -> float:
    if not (predictions_dir / "hard_threshold.npy").exists():
        hard_threshold = compute_hard_threshold(*args, **kwargs)
        np.save(predictions_dir / "hard_threshold.npy", np.array([hard_threshold]))
        logger.info(f"Hard threshold: {hard_threshold:.2f}. Stored at '{predictions_dir / 'hard_threshold.npy'}'")
    else:
        hard_threshold = np.load(predictions_dir / "hard_threshold.npy")[0]
        logger.info(f"Hard threshold: {hard_threshold:.2f}. Loaded from '{predictions_dir / 'hard_threshold.npy'}'")
    return hard_threshold

def compute_soft_threshold(bins_l2: NDArray["n_bins"], l2_all: NDArray["n,h,w,c"], cons_all: NDArray["n,h,w,c"],
                           n_sample: int) -> (float, float):
    l2_valid, cons_valid = _get_l2_cons_valid(l2_all, cons_all, n_sample)
    # l2_bin_ix = np.zeros_like(l2_valid)
    n_bins = len(bins_l2)
    # for each l2_valid, get l2_bin_ix based on the bins_l2
    l2_bin_dict = {}
    for i in range(n_bins):
        cond = l2_valid >= bins_l2[i]
        cond = cond & (l2_valid < bins_l2[i + 1]) if i < n_bins - 1 else cond
        l2_bin_dict[i] = cons_valid[cond]
    # keep only median items for each bin
    median = np.median([len(v) for v in l2_bin_dict.values()]).astype(int)
    l2_bin_dict_sampled = {k: np.random.choice(v, min(median, len(v)), replace=False) for k, v in l2_bin_dict.items()}
    # Perform linear regression on the sampled data
    # linear (exp) regression using OLS
    # exp(ax+b) = 1 - bin_i / n_bins => ax + b = ln(1 - bin_i / n_bins)
    x_cons = np.concatenate([x for x in l2_bin_dict_sampled.values()])
    gt_l2_bin = np.concatenate([np.repeat(k, len(v)) for k, v in l2_bin_dict_sampled.items()])
    gt_log = np.log(1 - gt_l2_bin / n_bins)
    lr = LinearRegression().fit(x_cons.reshape(-1, 1), gt_log)
    a, b = lr.coef_[0], lr.intercept_
    return a, b

def get_soft_threshold(predictions_dir: Path, *args, **kwargs) -> (float, float):

    file_path = predictions_dir / "soft_threshold.npy"
    if not file_path.exists():
        a, b = compute_soft_threshold(*args, **kwargs)
        np.save(file_path, np.array([a, b]))
        logger.info(f"Soft threshold: a={a:.2f}, b={b:.2f}. Stored at '{file_path}'")
    else:
        a, b = np.load(file_path)
        logger.info(f"Soft threshold: a={a:.2f}, b={b:.2f}. Loaded from '{file_path}'")
    return a, b

def compute_hard_perc_threshold(cons_all: NDArray["n,h,w,c"], l2_all: NDArray["n,h,w,c,"],
                                percentiles: list[int]) -> dict[float, float]:
    """
    Computes the hard thresholds for some top percentiles.
    """
    mask = np.isnan(l2_all)
    cons_valid = cons_all[~mask]
    res = dict(zip(percentiles, np.percentile(cons_valid, percentiles).tolist()))
    return res

def get_hard_perc_threshold(predictions_dir: Path, *args, **kwargs) -> dict[float, float]:
    """
    Computes the hard thresholds for some top percentiles.
    """
    file_path = predictions_dir / "hard_percentiles_threshold.npy"
    if not file_path.exists():
        res = compute_hard_perc_threshold(*args, **kwargs)
        np.save(file_path, res)
        logger.info(f"Computing hard percentiles threshold. Stored at '{file_path}'")
    else:
        res = np.load(file_path, allow_pickle=True).item()
        logger.info(f"Loading hard percentiles threshold from '{file_path}'")
    return res

def compute_hard_below_line_threshold(l2_all: NDArray["n,h,w,c"], cons_all: NDArray["n,h,w,c"], percentiles: list[int],
                                      avg_l2_per_cons_bin: NDArray["n_bins"],
                                      bins_cons: NDArray["n_bins"]) -> dict[float, float]:
    hard_threshold = compute_hard_threshold(l2_all, avg_l2_per_cons_bin, bins_cons)
    mask = np.isnan(l2_all)
    cons_valid = cons_all[~mask]
    thresholds = np.percentile(cons_valid[cons_valid <= hard_threshold], percentiles)
    res = dict(zip(percentiles, thresholds.tolist()))
    return res

def get_hard_below_line_threshold(predictions_dir: Path, *args, **kwargs) -> dict[float, float]:
    out_file = predictions_dir / "hard_below_line_percentiles_threshold.npy"
    if not out_file.exists():
        res = compute_hard_below_line_threshold(*args, **kwargs)
        np.save(out_file, res)
        logger.info(f"Computing hard below line threshold ({len(res)} percentiles). Stored at '{out_file}'")
    else:
        res = np.load(out_file, allow_pickle=True).item()
        logger.info(f"Loading hard below line threshold ({len(res)} percentiles) from '{out_file}'")
    return res

def get_args():
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("predictions_dir", type=Path)
    parser.add_argument("--gt_dir", type=Path)
    parser.add_argument("--only_consistency", action="store_true", help="use this for pseudolabels on test set")
    parser.add_argument("--cons_k", type=int, default=3, help="How many neighboring frames to use for consistency")
    parser.add_argument("--n_sample", type=int, help="How many data points to sample for the final plot")
    parser.add_argument("--n_bins", type=int, default=100)

    parser.add_argument("--cons_error_and_magnitude", action="store_true")
    parser.add_argument("--plot_cons_vs_err", action="store_true")

    args = parser.parse_args()
    assert args.predictions_dir.exists(), args.predictions_dir
    assert (args.gt_dir is not None and args.gt_dir.exists()) or args.only_consistency, args.gt_dir
    return args

def main():
    """main fn"""
    args = get_args()

    y_data, y_files = read_from_dir(args.predictions_dir)

    cons_all = _get_cons(args.predictions_dir, y_data, y_files, cons_k=args.cons_k)
    if args.only_consistency:
        logger.info("Only computing consistency, no thresholding")
        exit()

    gt_data, _ = read_from_dir(args.gt_dir)
    l2_all = compute_l2(y_data, gt_data)

    if args.cons_error_and_magnitude:
        out_dir = Path(__file__).parent / "cons_vs_l2" / y_files[0].parent.name
        cons_and_error_magnitude(l2_all, cons_all, out_dir=out_dir)

    if args.plot_cons_vs_err:
        plot_cons_vs_err(l2_all, cons_all, out_dir=args.predictions_dir, n_bins=args.n_bins,
                         n_sample=args.n_sample, plot_title=args.gt_dir.stem, enable_scatter=False)

    res = _get_bins_on_freqs(args.predictions_dir, l2_all, cons_all, n_sample=args.n_sample, n_bins=args.n_bins)
    freqs, (bins_cons, bins_l2), (avg_l2_per_cons_bin, avg_cons_per_l2_bin) = res

    # hard_threshold = get_hard_threshold(args.predictions_dir, l2_all, avg_l2_per_cons_bin, bins_cons)
    # soft_threshold = get_soft_threshold(args.predictions_dir, bins_l2, l2_all, cons_all, args.n_sample)
    # hard_perc_threshold = get_hard_perc_threshold(args.predictions_dir, cons_all, l2_all,
    #                                               percentiles=np.arange(1, 101))
    hard_perc_below_line_threshold = get_hard_below_line_threshold(args.predictions_dir, l2_all, cons_all,
                                                                   percentiles=np.arange(1, 101),
                                                                   avg_l2_per_cons_bin=avg_l2_per_cons_bin,
                                                                   bins_cons=bins_cons)

if __name__ == "__main__":
    main()
