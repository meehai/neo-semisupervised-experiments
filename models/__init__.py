"""Init file"""
from omegaconf import DictConfig
from torch import nn

from .safeuav import SafeUAV

def build_model(cfg: DictConfig, in_channels: int, out_channels: int) -> nn.Module:
    """builds a model from the config file based on type/params"""
    if cfg.model.type == "SafeUAV":
        return SafeUAV(in_channels=in_channels, out_channels=out_channels, **cfg.model.parameters)
    raise KeyError(f"Unknown model type: {cfg.model}")
