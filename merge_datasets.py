#!/usr/bin/env python3
"""merges two npy datasets on the common nodes"""

from pathlib import Path
from argparse import ArgumentParser
import shutil
import os
from loguru import logger

def _get_common_nodes(dataset1_path: Path, dataset2_path: Path) -> list[str]:
    """gets the common nodes"""
    d1_nodes = [x.name for x in dataset1_path.iterdir() if x.is_dir()]
    d2_nodes = [x.name for x in dataset2_path.iterdir() if x.is_dir()]
    common_nodes = set(d1_nodes).intersection(d2_nodes)
    assert len(common_nodes) >= 2, f"{d1_nodes} & {d2_nodes} => {common_nodes}. Must be at least 2."
    logger.info(f"Found common nodes: {common_nodes}")

    # check that nodes don't have any intersection at the file level
    d1_files = {node: [x.name for x in (dataset1_path / node).iterdir() if x.is_file()] for node in common_nodes}
    d2_files = {node: [x.name for x in (dataset2_path / node).iterdir() if x.is_file()] for node in common_nodes}
    for node in common_nodes:
        d1_node_files = d1_files[node]
        d2_node_files = d2_files[node]
        common = set(d1_node_files).intersection(d2_node_files)
        assert common == set(), f"Node: '{node}'. Common: {common}. Must be empty"
    return common_nodes

def get_args():
    parser = ArgumentParser()
    parser.add_argument("dataset1_path", type=Path)
    parser.add_argument("dataset2_path", type=Path)
    parser.add_argument("--output_path", "-o", type=Path)
    parser.add_argument("--overwrite", action="store_true")
    args = parser.parse_args()
    assert not args.output_path.exists() or args.overwrite, f"'{args.output_path}' already exists. Use --overwrite"
    assert args.dataset1_path.exists(), f"'{args.dataset1_path}' does not exist"
    assert args.dataset2_path.exists(), f"'{args.dataset2_path}' does not exist"
    if args.output_path.exists():
        logger.info(f"'{args.output_path}' exists. Removing.")
        shutil.rmtree(args.output_path)
    return args

def main():
    args = get_args()
    common_nodes = _get_common_nodes(args.dataset1_path, args.dataset2_path)
    args.output_path.mkdir(exist_ok=False, parents=True)

    for node in common_nodes:
        (args.output_path / node).mkdir(exist_ok=False)
        for file in (args.dataset1_path / node).iterdir():
            os.symlink(file.absolute(), args.output_path / node / file.name)
        for file in (args.dataset2_path / node).iterdir():
            os.symlink(file.absolute(), args.output_path / node / file.name)
    logger.info(f"Done. Merged common nodes at : '{args.output_path}'")

if __name__ == "__main__":
    main()