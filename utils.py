"""utils file for neo semisup algorithms repo"""
from pathlib import Path
from argparse import Namespace
from datetime import datetime
from torch import nn
from omegaconf import DictConfig
import torch as tr
from ngclib.logger import logger

def accelerator_params_from_cfg(cfg: DictConfig) -> dict:
    """get the accelerator params. Used in pl.Trainer constructor. TODO: move to lme"""
    accelerator = "gpu" if tr.cuda.is_available() else "cpu"
    single_device_ix = [0] if accelerator == "gpu" else 1
    if "accelerator_params" not in cfg.train:
        # single gpu/cpu
        return {"accelerator": accelerator, "devices": single_device_ix}
    cfg_acc = cfg.train.accelerator_params
    if cfg_acc.multi_gpu:
        logger.info(f"Using Multi GPUs.")
        devices = cfg_acc.get("devices", -1)
        return {"accelerator": "gpu", "devices": devices, "strategy": "ddp_find_unused_parameters_false"}

    return {"accelerator": accelerator, "devices": single_device_ix}

def get_experiment_dir(args: Namespace, cfg: DictConfig) -> Path:
    """
    Returns the experiment dir. If provided via --experiment_dir it's used as-is, otherwise it's generated based on
    the timestamp.
    """
    
    if args.experiment_dir is not None:
        return args.experiment_dir

    if "experiment_name" not in cfg:
        cfg.experiment_name = args.config_path.stem
        logger.warning(f"Experiment name not provided. Defaulting to '{cfg.experiment_name}'")

    timestamp1 = datetime.strftime(datetime.now(), "%Y%m%d")
    timestamp2 = datetime.strftime(datetime.now(), "%H%M%S")
    experiment_dir: Path = Path(__file__).parent / "experiments" / cfg.experiment_name / timestamp1 / timestamp2
    return experiment_dir
