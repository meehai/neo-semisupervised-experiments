# Semi-supervised learning algorithms using NEO data

Uses `NGCNpzReader` of [ngclib](https://gitlab.com/neural-graph-consensus/ngclib) for the reader.

NEO data from: [here](https://gitlab.com/meehai/datasets-scripts/-/tree/master/nasa-earth-observation).

## Steps:

- `EXPERIMENT_DIR=experiments/some_experiment` # if not set, will default to experiments/cfg/supervised/<timestamp>
- `CFG_FILE=cfgs/*/cfg.yaml` # pick your desired cfg (i.e. CHOLRA->AOD)

### 1. Train supervised

#### 1.1 Train the model
```
CUDA_VISIBLE_DEVICES=0 ./train.py \
    supervised
    --train_set_path /path/to/neo_data/dataset/train_set/ \
    --validation_set_path /path/to/neo_data/dataset/test_set \
    --config_path $CFG_FILE \
    --nodes_path /path/to/ngclib/nodes_repository/nasa/ \
    --experiment_dir $EXPERIMENT_DIR
```

#### 1.2 Evaluate the model
```
./evaluate.py \
    --test_set_path /path/to/neo_data/dataset/test_set/ \
    --config_path $CFG_FILE \
    --weights_path $EXPERIMENT_DIR/supervised/checkpoints/model_best.ckpt \
    --nodes_path ~/libs/ngclib/nodes_repository/nasa/ \
    -o results_supervised.csv
```

### 2. Train semi-supervised (baseline algorithm)

#### 2.1 Export pseudolabels

```
./pseudolabels.py \
    --semisupervised_set_path /path/to/semisupervised_set \
    --config_path $CFG_FILE \
    --experiment_dir $EXPERIMENT_DIR
    --nodes_path /path/to/ngclib/nodes_repository/nasa/
```

#### 2.2 Merge datasets (train set and pseudolabels)

```
./merge_datasets.py \
    /path/to/neo_data/dataset/train_set/ \
    --experiment_dir $EXPERIMENT_DIR/pseudolabels \
    -o $EXPERIMENT_DIR/semisupervised_set
```

#### 2.3 Train the model

```
CUDA_VISIBLE_DEVICES=0 ./train.py \
    semisupervised_simple
    --train_set_path $EXPERIMENT_DIR/semisupervised_set \
    --validation_set_path /path/to/neo_data/dataset/test_set \
    --config_path $CFG_FILE \
    --nodes_path /path/to/ngclib/nodes_repository/nasa/ \
    --experiment_dir $EXPERIMENT_DIR
```

#### 2.4 Evaluate the model

```
./evaluate.py \
    --test_set_path /path/to/neo_data/dataset/test_set/ \
    --config_path $CFG_FILE \
    --weights_path $EXPERIMENT_DIR/semisupervised_simple/checkpoints/model_best.ckpt \
    --nodes_path ~/libs/ngclib/nodes_repository/nasa/ \
    -o results_semisupervised_simple.csv
```

### 3. Train semi-supervised hard threshold

Note: this node must be ran for each output node in part. We'll assume only **one** output node, not a multi-task setup.
`export OUT_NODE=...`

#### 3.1 Generate pseudolabels on the train set

```
./pseudolabels.py \
    --semisupervised_set_path /path/to/neo_data/dataset/train_set/ \
    --config_path $CFG_FILE \
    --experiment_dir $EXPERIMENT_DIR
    --nodes_path /path/to/ngclib/nodes_repository/nasa/ \
    --pseudolabels_dir_name pseudolabels_train
```
#### 3.2 Run the variance x error code on the train pseudolabels export

```
./compute_consistency_vs_error.py \
    $EXPERIMENT_DIR/pseudolabels_train/$OUT_NODE/ \
    --gt_dir ~/datasets/neo_data/dataset/train_set/$OUT_NODE \
    --n_sample 30000 \ # if not set, will use the entire dataset
    --n_bins 100 # if not used, will default to 10
```

This script will output a bunch of files:
- `consistency.npy` - consistency array for each prediction pixel
- `freqs_bins_avgs_nbins_{n_bins}.pickle` - frequencies (bin_l2, bin_cons), plus average bin errors/consistencies
- `cons_vs_l2.png` and `l2_vs_cons.png` - plots of consistency bins vs erros (and vice versa)
- `hard_threshold.npy` - the hard threshold as observed in `cons_vs_l2.png`
- `soft_threshold.npy` - the soft threshold based on an exponential linear regression and the l2 bins.

#### 3.3 Generate consistency of the pseudolabels

```
./compute_consistency_vs_error.py \
    $EXPERIMENT_DIR/pseudolabels/$OUT_NODE/ \
    --only_consistency
```

#### 3.4 Copy the consistency.pickle and hard_threshold.npy file to the semisupervised dir

```
cp $EXPERIMENT_DIR/pseudolabels_train/$OUT_NODE/hard_threshold.npy $EXPRIMENT_DIR/semisupervised_set
cp $EXPERIMENT_DIR/pseudolabels/$OUT_NODE/consistency.pickle $EXPRIMENT_DIR/semisupervised_set
```

#### 3.5 Train the model

```
CUDA_VISIBLE_DEVICES=0 ./train.py \
    semisupervised_hard_threshold
    --train_set_path $EXPERIMENT_DIR/semisupervised_set \
    --validation_set_path /path/to/neo_data/dataset/test_set \
    --config_path $CFG_FILE \
    --nodes_path /path/to/ngclib/nodes_repository/nasa/ \
    --experiment_dir $EXPERIMENT_DIR
```

#### 3.6 Evaluate the model

```
./evaluate.py \
    --test_set_path /path/to/neo_data/dataset/test_set/ \
    --config_path $CFG_FILE \
    --weights_path $EXPERIMENT_DIR/semisupervised_hard_threshold/checkpoints/model_best.ckpt \
    --nodes_path ~/libs/ngclib/nodes_repository/nasa/ \
    -o results_semisupervised_hard_threshold.csv
```

### 4. Train semi-supervised soft threshold

Steps 4.1, 4.2, 4.3 are identical to 3.1, 3.2, 3.3, so if they were ran beforehand, no need to repeat

#### 4.4 Copy the consistency.pickle and soft_threshold.npy file to the semisupervised dir

```
cp $EXPERIMENT_DIR/pseudolabels/$OUT_NODE/consistency.pickle $EXPRIMENT_DIR/semisupervised_set
cp $EXPERIMENT_DIR/pseudolabels_train/$OUT_NODE/soft_threshold.npy $EXPRIMENT_DIR/semisupervised_set
```
#### 4.5 Train the model

```
CUDA_VISIBLE_DEVICES=0 ./train.py \
    semisupervised_soft_threshold
    --train_set_path $EXPERIMENT_DIR/semisupervised_set \
    --validation_set_path /path/to/neo_data/dataset/test_set \
    --config_path $CFG_FILE \
    --nodes_path /path/to/ngclib/nodes_repository/nasa/ \
    --experiment_dir $EXPERIMENT_DIR
```

#### 4.6 Evaluate the model

```
./evaluate.py \
    --test_set_path /path/to/neo_data/dataset/test_set/ \
    --config_path $CFG_FILE \
    --weights_path $EXPERIMENT_DIR/semisupervised_soft_threshold/checkpoints/model_best.ckpt \
    --nodes_path ~/libs/ngclib/nodes_repository/nasa/ \
    -o results_semisupervised_soft_threshold.csv
```

That's all.
