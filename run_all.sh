#!/usr/bin/bash
set -e
# cfgs/single_links/safeuav_nf16_ndvi_lai.yaml
# cfgs/single_links/safeuav_nf16_lstd_lstd_an.yaml
# cfgs/single_links/safeuav_nf16_lstn_lstn_an.yaml
# cfgs/single_links/safeuav_nf16_cld_fr_wv.yaml

test $# -eq 1 || { echo "Usage: $0 <config_path>"; exit 1; }

CFG_PATH=$1
TRAIN_SET_PATH=/export/home/proiecte/aux/mihai_cristian.pirvu/datasets/neo_data/dataset/train_set
TEST_SET_PATH=/export/home/proiecte/aux/mihai_cristian.pirvu/datasets/neo_data/dataset/test_set
NODES_PATH=/export/home/proiecte/aux/mihai_cristian.pirvu/libs/ngclib/nodes_repository/nasa/
STEM=$(basename $(ls $CFG_PATH) .yaml)
EXPERIMENT_DIR=experiments/$STEM/final

# Supervised training
CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7 ./train.py \
    supervised \
    --train_set_path $TRAIN_SET_PATH \
    --validation_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --nodes_path $NODES_PATH \
    --num_trains 8 \
    --experiment_dir $EXPERIMENT_DIR

./evaluate.py \
    --test_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --weights_path $EXPERIMENT_DIR/supervised/checkpoints/model_best.ckpt \
    --nodes_path $NODES_PATH

# hack if i've ever seen one. Only valid for 1 in, 1 out. Only valid after ./evaluate is ran.
OUT_NODE=$(cat $EXPERIMENT_DIR/supervised/results.csv | head -n 1 | cut -d "," -f2)

# Pseudolabels generation using supervised ckpt
./pseudolabels.py \
    --semisupervised_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --experiment_dir $EXPERIMENT_DIR \
    --nodes_path $NODES_PATH

./merge_datasets.py \
    $TRAIN_SET_PATH \
    $EXPERIMENT_DIR/pseudolabels/ \
    -o $EXPERIMENT_DIR/semisupervised_set \
    --overwrite

# thresholding generation on train pseudolabels
./pseudolabels.py \
    --semisupervised_set_path $TRAIN_SET_PATH \
    --config_path $CFG_PATH \
    --experiment_dir $EXPERIMENT_DIR \
    --nodes_path $NODES_PATH \
    --pseudolabels_dir_name pseudolabels_train

./compute_consistency_vs_error.py \
    $EXPERIMENT_DIR/pseudolabels_train/$OUT_NODE/ \
    --gt_dir $TRAIN_SET_PATH/$OUT_NODE/

./compute_consistency_vs_error.py \
    $EXPERIMENT_DIR/pseudolabels/$OUT_NODE/ \
    --only_consistency

cp \
    $EXPERIMENT_DIR/pseudolabels_train/$OUT_NODE/soft_threshold.npy \
    $EXPERIMENT_DIR/semisupervised_set/$OUT_NODE

cp \
    $EXPERIMENT_DIR/pseudolabels_train/$OUT_NODE/hard_threshold.npy \
    $EXPERIMENT_DIR/semisupervised_set/$OUT_NODE

cp \
    $EXPERIMENT_DIR/pseudolabels_train/$OUT_NODE/hard_percentiles_threshold.npy \
    $EXPERIMENT_DIR/semisupervised_set/$OUT_NODE

cp \
    $EXPERIMENT_DIR/pseudolabels/$OUT_NODE/consistency.pickle \
    $EXPERIMENT_DIR/semisupervised_set/$OUT_NODE

# semi-supervised sinple training

CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7 ./train.py \
    semisupervised_simple \
    --train_set_path $EXPERIMENT_DIR/semisupervised_set \
    --validation_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --nodes_path $NODES_PATH \
    --num_trains 8 \
    --experiment_dir $EXPERIMENT_DIR

./evaluate.py \
    --test_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --weights_path $EXPERIMENT_DIR/semisupervised_simple/checkpoints/model_best.ckpt \
    --nodes_path $NODES_PATH

# semi-supervised hard threshold training

CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7 ./train.py \
    semisupervised_hard_threshold \
    --train_set_path $EXPERIMENT_DIR/semisupervised_set \
    --validation_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --nodes_path $NODES_PATH \
    --num_trains 8 \
    --experiment_dir $EXPERIMENT_DIR

./evaluate.py \
    --test_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --weights_path $EXPERIMENT_DIR/semisupervised_hard_threshold/checkpoints/model_best.ckpt \
    --nodes_path $NODES_PATH

# semi-supervised soft threshold training

CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7 ./train.py \
    semisupervised_soft_threshold \
    --train_set_path $EXPERIMENT_DIR/semisupervised_set \
    --validation_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --nodes_path $NODES_PATH \
    --num_trains 8 \
    --experiment_dir $EXPERIMENT_DIR

./evaluate.py \
    --test_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --weights_path $EXPERIMENT_DIR/semisupervised_soft_threshold/checkpoints/model_best.ckpt \
    --nodes_path $NODES_PATH

# semi-supervised hard percentiles threshold training

CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7 ./train.py \
    semisupervised_hard_percentiles_threshold \
    --train_set_path $EXPERIMENT_DIR/semisupervised_set \
    --validation_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --nodes_path $NODES_PATH \
    --num_trains 8 \
    --experiment_dir $EXPERIMENT_DIR

./evaluate.py \
    --test_set_path $TEST_SET_PATH \
    --config_path $CFG_PATH \
    --weights_path $EXPERIMENT_DIR/semisupervised_hard_percentiles_threshold/checkpoints/model_best.ckpt \
    --nodes_path $NODES_PATH
